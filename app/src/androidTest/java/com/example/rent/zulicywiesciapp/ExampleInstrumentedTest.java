package com.example.rent.zulicywiesciapp;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.rent.zulicywiesciapp.model.FakeNewsListFactory;
import com.example.rent.zulicywiesciapp.model.NewsItem;

import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.junit.Assert.*;


/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class ExampleInstrumentedTest {

    @Rule
    public ActivityTestRule<NewsItemActivity> mActivityRule =
            new ActivityTestRule<>(NewsItemActivity.class, false, false);

    @Test
    public void useAppContext() throws Exception {
        NewsItem newsItem = FakeNewsListFactory.getFakeNewsList(5).get(0);
        Intent newsItemActivity = new Intent();
        newsItemActivity.putExtra("news_data", new String[]{
                String.valueOf(12345L),
                String.valueOf(newsItem.getContent()),
                String.valueOf(newsItem.getDate()),
                String.valueOf(newsItem.getImg_url()),
                String.valueOf(newsItem.getPriority()),
                String.valueOf(newsItem.getTitle()),
                String.valueOf(newsItem.getAuthor().getName())
        });
        mActivityRule.launchActivity(newsItemActivity);

    }
}
